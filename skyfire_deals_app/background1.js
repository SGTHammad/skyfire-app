var site_dict;
var domain;

function getSiteDict(){
	return site_dict;
}
function getDomain(){
	return domain;
}
function parseDomain(url){
	var matches = url.match(/^https?\:\/\/([^\/:?#]+)(?:[\/:?#]|$)/i);
	domain = matches && matches[1];
	return domain;
}
var toolbar_fun = {
	onPageChanged: function(url) {
		if (site_dict) {
			if (site_dict.original_url == url){
				console.log('on same site, not going to do whole onload again!');
				return;
			}
		}
		domain = parseDomain(url);
		//toolbar_extension.clearNotification();
		var deal_request_url = "http://api.socialingot.com/skyfire/deals?url=" + url;
		$.get(deal_request_url, function(data) {
			var response = $.parseJSON(data);
			site_dict = response;
			var deals = response.deals;
			var num_of_deals = deals.length;
			var message = num_of_deals + site_dict.labels.deals_notification;
			toolbar_ui.showActionableAlert(num_of_deals, message, 'onAlertClick');
			toolbar_extension.setNotification(String(num_of_deals));
		});
	}
};

function onAlertClick(){ 
	toolbar_extension.open(); 
}

function onPopupOpen(){
	toolbar_extension.clearNotification();
	toolbar_extension.setIcon('xhdpi/deal75.png')
}


function onPopupClose(){
	toolbar_extension.setIcon('xhdpi/75grey.png')
}

toolbar_browser.setOnPageChangeListener('toolbar_fun.onPageChanged');
toolbar_extension.setOnOpenListener('onPopupOpen');
toolbar_extension.setOnCloseListener('onPopupClose');

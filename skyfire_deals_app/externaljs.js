var siteDict = window.parent.getSiteDict;

if (siteDict === undefined){
	console.log('sitedict was undefined');
	getDeals();
	
} else if ($.isEmptyObject(siteDict())){
	console.log('sitedict was empty');
	getDeals();
	//showNoDeals();
} else {
	console.log('sitedict was valid');
	window.onload = function() {
		loadDeals(siteDict());
	}
}

function showNoDeals(){
	hideLoadingSymbol();
	$("#no_deals").show();
	/*
	var noDeals = $("<div/>", {
		'class': 'app_header',
		'id': 'no_deals',
		'data-theme': 'd',
		'data-role': 'header',
	});
	var message = $("<h5/>");
	message.text("Sorry, we do not currently have any deals for this site, but check back soon!");
	noDeals.append(message);
	hideLoadingSymbol();
	$('#page1').append(noDeals).trigger('create');
	console.log('appending no deals div');
	*/
}

function getDeals(){
	var currentUrl = toolbar_browser.getUrl();
	console.log('getting url from toolbar: ' + currentUrl);
	var anotherUrl = window.parent.document.location.href;
	console.log('is the above the same asss: ' + anotherUrl);
	//var currentUrl = 'http://www.shoes.com';
	var deal_request_url = "http://gull.dyn-o-saur.com:9009/skyfire/deals?callback=?&url=" + currentUrl;
	$.get(deal_request_url, function(data) {
		siteDict = $.parseJSON(data);
		if ($.isEmptyObject(siteDict)){
			showNoDeals();
		} else {
			loadDeals(siteDict);
		}
		//var deals = response.deals;
		//var num_of_deals = deals.length;
		//var message = num_of_deals + local_site_dict.labels.deals_notification;
		//toolbar_ui.showActionableAlert(num_of_deals, message, 'onAlertClick');
		//toolbar_extension.setNotification(String(num_of_deals));
	});
}

function hideLoadingSymbol(){
	$('#loading_page').hide();
}

function showLoadingSymbol(){
	$('#loading_page').show();
}

function loadDeals(local_site_dict) {
    var num_of_deals = local_site_dict.deals.length;
    var merchant_name = local_site_dict.name;
    hideLoadingSymbol();
	makeAppHeader(local_site_dict);
	
	//$('#no_deals').hide();
	var types = local_site_dict.types;
	if(types.General){
		makeDealSection(types.General, local_site_dict.labels.general);
	}
	if(types.Coupon){
		makeDealSection(types.Coupon, local_site_dict.labels.coupons);	
	}
	if(types.FreeShipping){
		makeDealSection(types.FreeShipping, local_site_dict.labels.freeshipping);
	}
	if(types.Giveaway){
		makeDealSection(types.Giveaway, local_site_dict.labels.giveaways);
	}
     //$('#general').children['a'].attr('id','test');
};

function toggleClass(h3){
    var a_tag = $(h3).children('a')[0];
    if ($(a_tag).hasClass('no_border')){
        $(a_tag).removeClass('no_border');
    }
    else {
        $(a_tag).addClass('no_border');
    }
}

function makeDealSection(type, header_title){
	var num_of_deals = type.length;
	var deal_header_div = $('<div/>', 
				//{'class': 'ui-collapsible ui-collapsible-inset ui-corner-all ui-collapsible-themed-content ui-collapsible-collapsed', 
				{'data-role': 'collapsible', 
				'data-theme': 'a',
				'data-content-theme':'a'});	
	var collapsible_header = $('<h3/>', {
        'class':'ui-collapsible-heading ui-collapsible-heading-collapsed',
        'onclick': 'toggleClass(this);'
    });
	collapsible_header.text(header_title +" ("+num_of_deals+")");
	deal_header_div.append(collapsible_header).trigger('create');
	var deal_list = $('<ul/>', 
			{'data-role':'listview', 
			'data-divider-theme': 'b', 
			'data-inset': 'true',})
			//'class': 'section_header ui-listview ui-listview-inset ui-corner-all ui-shadow'})
	type.forEach(makeDealDiv, deal_list);
	deal_header_div.append(deal_list).trigger('create');
	$("#page1").append(deal_header_div).trigger('create');
}

function makeAppHeader(local_site_dict){
	var app_header_div = $('<div/>', 
				{'class': 'app_header ui-header ui-bar-d', 
				'data-role': 'header', 
				'role': 'banner',
				'aria-level': '1',});
	var app_header = $('<h5/>',
		{'class': 'ui-title', 
				'role': 'heading',
				'data-theme': 'd',});	
	app_header.text(local_site_dict.deals.length + local_site_dict.labels.deals_header + local_site_dict.name);
	app_header_div.append(app_header);
	$("#page1").append(app_header_div);
}

function makeDealDiv(deal, deal_index, deal_array){
	if (deal_index == 0){
		deal_element_class = 'ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-first-child ui-btn-up-c';
	} else if (deal_index == (deal_array.length - 1)){
		deal_element_class = 'ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-last-child ui-btn-up-c';
	} else {
		deal_element_class = 'ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c';
	}
	var deal_element = $('<li/>',  
		{'class': deal_element_class, 
		'data-theme':'c'})
	var deal_link = $('<a/>',  
		{'data-transition':'slide',
		'href': '#',
		'onclick': 'triggerClick("' + deal.url + '");'});
	deal_link.text(deal.text);
	deal_element.append(deal_link).trigger('create');
	this.append(deal_element).trigger('create');
};

function triggerClick(deal_url){
	toolbar_browser.loadUrl(deal_url);
}

function getDomain(url){
	var matches = url.match(/^https?\:\/\/([^\/:?#]+)(?:[\/:?#]|$)/i);
	var domain = matches && matches[1];
	return domain;
}